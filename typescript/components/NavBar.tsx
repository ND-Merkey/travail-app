import React from 'react'
import Image from 'next/image'
import logo from '../public/img/logo.png'

import {Avatar, AvatarImage, AvatarFallback} from '@/components/ui/avatar'
import Link from 'next/link'
import {
  CreditCard,
  LogOut,
  Settings,
  User,
} from "lucide-react"

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"

const NavBar = () => {
  return (
    <div className='bg-primary dark:bg-slate-700 text-white py-2 px-5 flex justify-between '>
      <Link href='/'>
        <Image src={logo} alt='TraverSpy' width={40} />
      </Link>

      <DropdownMenu>
      <DropdownMenuTrigger asChild className='focus::outline-none'>
        <Avatar>
          <AvatarImage
            src='https://github.com/shadcn.png'
            alt='User Avatar'
            width={32}
            height={32}
            className='cursor-pointer'
            
          />
          <AvatarFallback className='text-black'>User</AvatarFallback>
        </Avatar>
      </DropdownMenuTrigger>
      <DropdownMenuContent className="w-56">
        <DropdownMenuLabel>My Account</DropdownMenuLabel>
        <DropdownMenuSeparator />
          <DropdownMenuItem>
            <User className="mr-2 h-4 w-4" />
            <span>
              <Link href='/profile'>Profile</Link>
            </span>
            <DropdownMenuShortcut>⇧⌘P</DropdownMenuShortcut>
          </DropdownMenuItem>
        
        <DropdownMenuItem>
          <LogOut className="mr-2 h-4 w-4" />
          <span>
            <Link href='/logout'>Logout</Link>
          </span>
          <DropdownMenuShortcut>⇧⌘Q</DropdownMenuShortcut>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
      
    </div>
  )
}

export default NavBar
