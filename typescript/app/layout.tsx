import React from "react";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import './globals.css';

//components
import NavBar from "../components/NavBar";
import SideBar from "../components/Sidebar";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "TypeScript Project",
  description: "Integrate Typescript in the project",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${inter.className}`}>

         <NavBar />
         <div className="flex ">
            <div className="hidden md:block h-[100vh]
             md:w-[320px]">
            <SideBar />
            </div>
            <div className="p-5 w-full md:max-w-[1140px]">
            {children}
            </div>
         </div>
        </body>
    </html>
  );
}
