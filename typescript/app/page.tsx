import DashboadCard from "@/components/Dashboad/DashboadCard";
import PostTable from "@/components/posts/PostsTable";
import { Folder, MessageCircle, NewspaperIcon, User } from "lucide-react";

export default function Home() {
  return (
    <>
      <div className="flex flex-col md:flex-row justify-between gap-10 mb-4">
      <DashboadCard 
          title="Post" 
          count={100} 
          icon={<NewspaperIcon className="text-slate-500 dark:text-slate-200"  size={72}/>} 
          />
        <DashboadCard 
            title="Categories" 
            count={100} 
            icon={<Folder className="text-slate-500 dark:text-slate-200"  size={72}/>} 
          />
        <DashboadCard 
            title="Users" 
            count={100} 
            icon={<User className="text-slate-500 dark:text-slate-200"  size={72} />} 
          />
           <DashboadCard 
              title="Comment" 
              count={100} 
              icon={<MessageCircle className="text-slate-500 dark:text-slate-200"  size={72} />} 
          />
      </div>

      <PostTable limit={5} title='Latest Posts' />
    </>
  );
}